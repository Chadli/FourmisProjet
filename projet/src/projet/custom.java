package projet;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class custom extends JFrame {
	private JTextField jtfbarrier = new JTextField();
	private JTextField jtffood = new JTextField();
	private JTextField jtffrekence = new JTextField();
	private JTextField jtfant = new JTextField();


	private JComboBox jcbbox = new JComboBox();
	
	public JPanel initbox(){
		
		JPanel jp1 = new JPanel();
		jp1.setLayout(new FlowLayout());
		jp1.setBorder(BorderFactory.createTitledBorder("Have fun"));
		Box box1 = Box.createVerticalBox();
		JLabel jlbarrier = new JLabel("Barrier count: ");
		JLabel jlfood = new JLabel("Food count: ");
		JLabel jlfrekence = new JLabel("Frekence count: ");
		JLabel jlant = new JLabel("Ant count: ");

		JButton jbok = new JButton("OK");
		jbok.addActionListener(new actionconfirm());
		
		jlbarrier.setAlignmentX(Component.CENTER_ALIGNMENT);
		jtfbarrier.setAlignmentX(Component.CENTER_ALIGNMENT);
		jlfrekence.setAlignmentX(Component.CENTER_ALIGNMENT);
		jtffrekence.setAlignmentX(Component.CENTER_ALIGNMENT);
		jlant.setAlignmentX(Component.CENTER_ALIGNMENT);
		jtfant.setAlignmentX(Component.CENTER_ALIGNMENT);
		jlfood.setAlignmentX(Component.CENTER_ALIGNMENT);
		jtffood.setAlignmentX(Component.CENTER_ALIGNMENT);
		jbok.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		box1.add(jlbarrier);
		box1.add(jtfbarrier);
		box1.add(jlfrekence);
		box1.add(jtffrekence);
		box1.add(jlant);
		box1.add(jtfant);
		box1.add(jlfood);
		box1.add(jtffood);
		box1.add(jbok);
		
		jp1.add(box1);
		return jp1;
	}
	
	public custom(){
		this("Custom");
	}
	public custom(String titre){
		super(titre);
		Box boxPrincipale = Box.createHorizontalBox();

		JPanel jp1 = initbox();
		boxPrincipale.add(jp1);

		jtfbarrier.setText("0");
		jtffood.setText("0");
		jtffrekence.setText("0");
		jtfant.setText("0");
		
		add(boxPrincipale,BorderLayout.CENTER);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(250,250);
		setVisible(true);
		setLocationRelativeTo(null);


	}
	class actionconfirm implements ActionListener{
		
	
		public void actionPerformed(ActionEvent e){
			
			
		}
	}
	
	public static void main(String args[]){
		
		new custom();
	}
}

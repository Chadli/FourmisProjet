package projet1;

public class GTParameters {

	/**
	 * Radical parameter.
	 */
	public static final int WINDOW_WIDTH = 800;

	public static final int BASIC = WINDOW_WIDTH / 800;

	public static final int WINDOW_HEIGHT = BASIC * 500;

	public static final int SCALE = BASIC * 14;

	public static final Position START_POINT = new Position(WINDOW_WIDTH / SCALE / 2, 1, SCALE);

	public static final int RADIUS = BASIC * 40;


}

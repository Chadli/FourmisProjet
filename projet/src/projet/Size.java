package projet1;


import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class Size extends JPanel {
	private static final long serialVersionUID = -541698616292452515L;
	public Size(){}
	public void paint(Graphics g){
		super.paintComponent(g);
		g.setColor(Color.blue);
		for (int i = GTParameters.SCALE; i <= 1090; i += GTParameters.SCALE) {
			g.drawLine(i, 1, i, 1000);
		}

		for (int i = GTParameters.SCALE; i <= 1000; i += GTParameters.SCALE) {
			g.drawLine(1, i, 1090, i);
		}
	        
	
	}
}
